<?php
namespace PhalApi\Session\Drivers;
/**
 *  Session Database Driver
 *
 */
class Session_database_driver extends Session_driver implements SessionHandlerInterface {

	/**
	 * DB object
	 *
	 * @var	object
	 */
	protected $_db;

	/**
	 * Row exists flag
	 *
	 * @var	bool
	 */
	protected $_row_exists = FALSE;

	/**
	 * Lock "driver" flag
	 *
	 * @var	string
	 */
	protected $_platform;

	// ------------------------------------------------------------------------

	/**
	 * Class constructor
	 *
	 * @param	array	$params	Configuration parameters
	 * @return	void
	 */
	public function __construct(&$params)
	{
		parent::__construct($params);
        // 数据操作 - 基于NotORM
        $this->_db = new \PhalApi\Database\NotORMDatabase(\PhalApi\DI()->config->get('dbs'), \PhalApi\DI()->config->get('sys.notorm_debug'));

		$db_driver = strtolower(\PhalApi\DI()->config->get('session.driver_type'));
		if (strpos($db_driver, 'mysql') !== FALSE)
		{
			$this->_platform = 'mysql';
		}
		elseif (in_array($db_driver, array('postgre', 'pdo_pgsql'), TRUE))
		{
			$this->_platform = 'postgre';
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Open
	 *
	 * Initializes the database connection
	 *
	 * @param	string	$save_path	Table name
	 * @param	string	$name		Session cookie name, unused
	 * @return	bool
	 */
	public function open($save_path, $name)
	{
        try{
            if (!is_object($this->_db)||is_null($this->_db)) {
                return $this->_fail();
            }
        }catch (\Exception $e){
            return $this->_fail();
        }

		$this->php5_validate_id();

		return $this->_success;
	}

	// ------------------------------------------------------------------------

	/**
	 * Read
	 *
	 * Reads session data and acquires a lock
	 *
	 * @param	string	$session_id	Session ID
	 * @return	string	Serialized session data
	 */
	public function read($session_id)
	{
		if ($this->_get_lock($session_id) !== FALSE)
		{
			// Needed by write() to detect session_regenerate_id() calls
			$this->_session_id = $session_id;
            // 获取一个NotORM实例
            $save_path = $this->_config['save_path'];
            $where = [];
            $where['id'] = $session_id;
			if ($this->_config['match_ip'])
			{
				$where['ip_address'] = $_SERVER['REMOTE_ADDR'];
			}
            $result = $this->_db
                ->$save_path
                ->select('data')
                ->where($where)
                ->fetchOne();
			if ( $result === false)
			{
				// PHP7 will reuse the same SessionHandler object after
				// ID regeneration, so we need to explicitly set this to
				// FALSE instead of relying on the default ...
				$this->_row_exists = FALSE;
				$this->_fingerprint = md5('');
				return '';
			}
			// PostgreSQL's variant of a BLOB datatype is Bytea, which is a
			// PITA to work with, so we use base64-encoded data in a TEXT
			// field instead.
			$result = ($this->_platform === 'postgre')
				? base64_decode(rtrim($result['data']))
				: $result['data'];

			$this->_fingerprint = md5($result);
			$this->_row_exists = TRUE;
			return $result;
		}

		$this->_fingerprint = md5('');
		return '';
	}

	// ------------------------------------------------------------------------

	/**
	 * Write
	 *
	 * Writes (create / update) session data
	 *
	 * @param	string	$session_id	Session ID
	 * @param	string	$session_data	Serialized session data
	 * @return	bool
	 */
	public function write($session_id, $session_data)
	{
        $save_path = $this->_config['save_path'];
		// Was the ID regenerated?
		if (isset($this->_session_id) && $session_id !== $this->_session_id)
		{
			if ( ! $this->_release_lock() OR ! $this->_get_lock($session_id))
			{
				return $this->_fail();
			}

			$this->_row_exists = FALSE;
			$this->_session_id = $session_id;
		}
		elseif ($this->_lock === FALSE)
		{
			return $this->_fail();
		}

		if ($this->_row_exists === FALSE)
		{
			$insert_data = array(
				'id' => $session_id,
				'ip_address' => $_SERVER['REMOTE_ADDR'],
				'timestamp' => time(),
				'data' => ($this->_platform === 'postgre' ? base64_encode($session_data) : $session_data)
			);

            $result = $this->_db
                ->$save_path
                ->insert($insert_data);
			if ($result->insert_id())
			{
				$this->_fingerprint = md5($session_data);
				$this->_row_exists = TRUE;
				return $this->_success;
			}

			return $this->_fail();
		}
        $where = [];
		$where['id'] = $session_id;
		if ($this->_config['match_ip'])
		{
			$where['ip_address'] = $_SERVER['REMOTE_ADDR'];
		}

		$update_data = array('timestamp' => time());
		if ($this->_fingerprint !== md5($session_data))
		{
			$update_data['data'] = ($this->_platform === 'postgre')
				? base64_encode($session_data)
				: $session_data;
		}
		if ($this->_db->$save_path->where($where)->update($update_data)!==false)
		{
			$this->_fingerprint = md5($session_data);
			return $this->_success;
		}

		return $this->_fail();
	}

	// ------------------------------------------------------------------------

	/**
	 * Close
	 *
	 * Releases locks
	 *
	 * @return	bool
	 */
	public function close()
	{
		return ($this->_lock && ! $this->_release_lock())
			? $this->_fail()
			: $this->_success;
	}

	// ------------------------------------------------------------------------

	/**
	 * Destroy
	 *
	 * Destroys the current session.
	 *
	 * @param	string	$session_id	Session ID
	 * @return	bool
	 */
	public function destroy($session_id)
	{
		if ($this->_lock)
		{
		    $where = [];
			$where['id'] = $session_id;
			if ($this->_config['match_ip'])
			{
				$where['ip_address'] = $_SERVER['REMOTE_ADDR'];
			}
            $save_path = $this->_config['save_path'];
			if ($this->_db->$save_path->where($where)->delete()===false)
			{
				return $this->_fail();
			}
		}

		if ($this->close() === $this->_success)
		{
			$this->_cookie_destroy();
			return $this->_success;
		}

		return $this->_fail();
	}

	// ------------------------------------------------------------------------

	/**
	 * Garbage Collector
	 *
	 * Deletes expired sessions
	 *
	 * @param	int 	$maxlifetime	Maximum lifetime of sessions
	 * @return	bool
	 */
	public function gc($maxlifetime)
	{
        $save_path = $this->_config['save_path'];
		return ($this->_db->$save_path->where('timestamp < ?',time() - $maxlifetime)->delete()!==false)
			? $this->_success
			: $this->_fail();
	}

	// --------------------------------------------------------------------

	/**
	 * Validate ID
	 *
	 * Checks whether a session ID record exists server-side,
	 * to enforce session.use_strict_mode.
	 *
	 * @param	string	$id
	 * @return	bool
	 */
	public function validateSessionId($id)
	{
        $save_path = $this->_config['save_path'];
        $where = [];
        $where['id'] = $id;
        empty($this->_config['match_ip']) OR $where['ip_address'] = $_SERVER['REMOTE_ADDR'];
		$count = $this->_db->$save_path->where($where)->count('1');
		return $count>0;
	}

	// ------------------------------------------------------------------------

	/**
	 * Get lock
	 *
	 * Acquires a lock, depending on the underlying platform.
	 *
	 * @param	string	$session_id	Session ID
	 * @return	bool
	 */
	protected function _get_lock($session_id)
	{
		if ($this->_platform === 'mysql')
		{
			$arg = md5($session_id.($this->_config['match_ip'] ? '_'.$_SERVER['REMOTE_ADDR'] : ''));

			if ($this->_db->session->query("SELECT GET_LOCK('".$arg."', 300) AS ci_session_lock",array())->fetchObject()->ci_session_lock)
			{
				$this->_lock = $arg;
				return TRUE;
			}

			return FALSE;
		}
		elseif ($this->_platform === 'postgre')
		{
			$arg = "hashtext('".$session_id."')".($this->_config['match_ip'] ? ", hashtext('".$_SERVER['REMOTE_ADDR']."')" : '');

			if ($this->_db->session->query('SELECT pg_advisory_lock('.$arg.')',array()))
			{
				$this->_lock = $arg;
				return TRUE;
			}

			return FALSE;
		}

		return parent::_get_lock($session_id);
	}

	// ------------------------------------------------------------------------

	/**
	 * Release lock
	 *
	 * Releases a previously acquired lock
	 *
	 * @return	bool
	 */
	protected function _release_lock()
	{
		if ( ! $this->_lock)
		{
			return TRUE;
		}

		if ($this->_platform === 'mysql')
		{

			if ($this->_db->session->query("SELECT RELEASE_LOCK('".$this->_lock."') AS ci_session_lock",array())->fetchObject()->ci_session_lock)
			{
				$this->_lock = FALSE;
				return TRUE;
			}

			return FALSE;
		}
		elseif ($this->_platform === 'postgre')
		{
			if ($this->_db->session->query('SELECT pg_advisory_unlock('.$this->_lock.')',array()))
			{
				$this->_lock = FALSE;
				return TRUE;
			}

			return FALSE;
		}

		return parent::_release_lock();
	}
}
