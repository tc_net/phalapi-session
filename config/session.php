<?php
/**
 * Created by PhpStorm.
 * User: tc-net
 * Date: 2019/9/18
 * Time: 16:26
 * session 配置
 */
return array(
    'driver'        =>  'database',     //files/database/redis/memcached/custom 使用的存储 session 的驱动
    'driver_type'   =>  'MYSQL',        //database时候mysql|postgre|pdo_pgsql
    'save_path'     =>  'session', //指定存储位置，取决于使用的存储 session 的驱动. database 时候为表名称，如果dbs配置前缀了前缀，这里不需要前缀;redis为tcp://127.0.0.1:5500;null
    'cookie_name'   =>  'foundao_sid',  //[A-Za-z_-] characters only session cookie 的名称
    'cookie_path'   =>  '/',            //session 可用的域 ，也可以在实例化时候船速
    'cookie_domain' =>  '',            //session 可用的路径
    'cookie_secure' =>  FALSE,             //是否只在加密连接（HTTPS）时创建 session cookie
    'expiration'    =>   0,  //Time in seconds (integer) 你希望 session 持续的秒数 如果你希望 session 不过期（直到浏览器关闭），将其设置为 0
    'match_ip'      =>   FALSE,     //TRUE/FALSE (boolean) 读取 session cookie 时，是否验证用户的 IP 地址 注意有些 ISP 会动态的修改 IP ，所以如果你想要一个不过期的 session，将其设置为 FALSE
    'time_to_update'        => 0,      //Time in seconds (integer)	该选项用于控制过多久将重新生成一个新 session ID 设置为 0 将禁用 session ID 的重新生成
    'regenerate_destroy'    => FALSE, //TRUE/FALSE (boolean)	当自动重新生成 session ID 时，是否销毁老的 session ID 对应的数据 如果设置为 FALSE ，数据之后将自动被垃圾回收器删除
);