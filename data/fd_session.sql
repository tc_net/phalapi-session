/*
Navicat MySQL Data Transfer
Date: 2019-09-19 10:59:37
*/
-- ----------------------------
-- Table structure for fd_session
-- ----------------------------
DROP TABLE IF EXISTS `fd_session`;
CREATE TABLE `fd_session` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
